package com.example.julian.background;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.media.RingtoneManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Background extends Service {
    private Thread repeatTaskThread;

    Handler handler = new Handler();
    Runnable turnBlack = new Runnable() {
        @Override
        public void run() {
            FirebaseDatabase data = FirebaseDatabase.getInstance();
            DatabaseReference MaxDatabase = data.getReference();
            MaxDatabase.child("Version Data").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot answerSnapshot : dataSnapshot.getChildren()) {

                        if (answerSnapshot.getKey().equals("trigger")) {
                            System.out.println(answerSnapshot.getValue(String.class));
                            handler.postDelayed(turnBlack, 5000);
                            if (answerSnapshot.getValue(String.class).equals("no")) {
                                SendNotification("haha");
                                System.out.println("Notification Send");
                            }

                        }

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    };


    @Override
    public void onCreate() {
        System.out.println("Service Started");

    }

    /** The service is starting, due to a call to startService() */
    @Override
    // Let it continue running until it is stopped.
    public int onStartCommand(Intent intent, int flags, int startId) {
        handler.postDelayed(turnBlack, 5000);
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /** Called when The service is no longer used and is being destroyed */
    @Override
    public void onDestroy() {
        super.onDestroy();
        System.out.println("Service Stopped");


    }


    public void SendNotification(String a) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(Background.this, "1")
                      //  .setSmallIcon(R.mipmap.heart) //icon
                        .setContentTitle("Test")
                        .setContentText("Test")
                        .setAutoCancel(true)//swipe for delete
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(1, mBuilder.build());            }

        };
        Thread thread = new Thread(runnable);
        thread.start();
    }


    private class Test extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference mdatabaseReference = database.getReference("Users/" + "TestID");
            mdatabaseReference.child("Name").setValue("TEST");
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
System.out.println(" TestID Added!");
// might want to change "executed" for the returned string passed
            // into onPostExecute() but that is upto you
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

}

